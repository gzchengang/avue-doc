<script>
export default {
  data() {
      return {
        option: {
          "borderWidth": 1,
          "type": 0,
          "borderColor": "#0dffff",
          "areaColor": "#fff",
          "banner": true,
          "bannerTime": 3000,
          "fontSize": 12,
          "zoom": 0.5,
          "empAreaColor": "#061d33",
          "color": "rgba(13, 255, 255, 1)",
          "mapData":'https://data.bladex.vip/blade-visual/map/data?id=1235113459258056705'
        },
         data: [{
          "name": "我是一个测试坐标",
          "value": 1,
          "lng": 113.2086181600,
          "lat": 40.9591597700,
          "zoom": 1
        }]
      }

    }
}
</script>


# EchartMap 地图
:::tip
1.1.0+
::::

```
<!-- 导入需要的包 -->  
<script src="https://cdnjs.cloudflare.com/ajax/libs/echarts/4.2.1/echarts.min.js"></script>
<!-- 地图数据包 --> 
https://datav.aliyun.com/tools/atlas/#
<!-- 自己绘制地图包 --> 
http://geojson.io/#map=2/20.0/0.0
```

:::demo 
```html
  <avue-echart-map ref="echart" :option="option" :data="data" width="1000"></avue-echart-map>
<script>
export default {
  data() {
      return {
        option: {
          "borderWidth": 1,
          "type": 0,
          "borderColor": "#0dffff",
          "areaColor": "#fff",
          "banner": true,
          "bannerTime": 3000,
          "fontSize": 12,
          "zoom": 0.6,
          "empAreaColor": "#061d33",
          "color": "rgba(13, 255, 255, 1)",
          "mapData":'https://data.bladex.vip/blade-visual/map/data?id=1235113459258056705'
        },
        data: [{
          "name": "我是一个测试坐标",
          "value": 1,
          "lng": 113.2086181600,
          "lat": 40.9591597700,
          "zoom": 1
        }]
      }

    }
}
</script>


```
:::




