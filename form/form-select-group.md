<script>
export default {
    data() {
      return {
        obj: {
          select:'Shanghai'
        },
        option: {
          column: [
            {
            label: '分组',
            prop: 'select',
            type: 'select',
            group: true,
            dicData: [{
              label: '热门城市',
              groups: [{
                value: 'Shanghai',
                label: '上海'
              }, {
                value: 'Beijing',
                label: '北京'
              }]
            }, {
              label: '城市名',
              groups: [{
                value: 'Chengdu',
                label: '成都'
              }, {
                value: 'Shenzhen',
                label: '深圳'
              }, {
                value: 'Guangzhou',
                label: '广州'
              }, {
                value: 'Dalian',
                label: '大连'
              }]
            }]
            }
          ]
        }
      }
    }
}
</script>
# 表单选择框分组

:::tip
 2.1.0+
::::

:::demo  配置`group`为`true`即可开启分组模式
```html
<avue-form :option="option" v-model="obj"></avue-form>
<script>
export default {
    data() {
      return {
        obj: {
         select:'Shanghai'
        },
        option: {
          column: [
            {
            label: '分组',
            prop: 'select',
            type: 'select',
            group: true,
            dicData: [{
              label: '热门城市',
              groups: [{
                value: 'Shanghai',
                label: '上海'
              }, {
                value: 'Beijing',
                label: '北京'
              }]
            }, {
              label: '城市名',
              groups: [{
                value: 'Chengdu',
                label: '成都'
              }, {
                value: 'Shenzhen',
                label: '深圳'
              }, {
                value: 'Guangzhou',
                label: '广州'
              }, {
                value: 'Dalian',
                label: '大连'
              }]
            }]
            }
          ]
        }
      }
    }
}
</script>

```
:::

