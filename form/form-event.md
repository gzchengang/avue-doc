<script>
export default {
 data() {
      return {
        obj: {
          name: '11'
        },
        data: [],
        option: {
          column: [{
            label: '姓名',
            prop: 'name',
            change: ({value,column}) => {
              this.$message.success('查看控制台',value,column)
              console.log('值改变')
            },
            click: ({value,column}) => {
              this.$message.success('查看控制台',value,column)
              console.log('点击事件')
            },
            focus: ({value,column}) => {
              this.$message.success('查看控制台',value,column)
              console.log('获取焦点')
            },
            blur: ({value,column}) => {
              this.$message.success('查看控制台',value,column)
              console.log('失去焦点')
            }
          }]
        }
      }
    }
}
</script>
# 组件事件
:::tip
2.6.0+
::::

- `change`事件
- `click`事件
- `focus`事件
- `blur`事件

## 普通用法 
:::demo  目前组件有4个事件`change`,`click`,`focus`,`blur`
```html
 <avue-form :option="option" v-model="obj"></avue-form>
<script>
export default {
 data() {
      return {
        obj: {
          name: '11'
        },
        data: [],
        option: {
          column: [{
            label: '姓名',
            prop: 'name',
            change: ({value,column}) => {
              this.$message.success('查看控制台',value,column)
              console.log('值改变')
            },
            click: ({value,column}) => {
              this.$message.success('查看控制台',value,column)
              console.log('点击事件')
            },
            focus: ({value,column}) => {
              this.$message.success('查看控制台',value,column)
              console.log('获取焦点')
            },
            blur: ({value,column}) => {
              this.$message.success('查看控制台',value,column)
              console.log('失去焦点')
            }
          }]
        }
      }
    }
}
</script>

```
:::

