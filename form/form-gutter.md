<script>
export default {
  data(){
    return {
       gutter:20,
       form:{}
    }
  },
  computed:{
    option(){
      return {
        card: false,
        labelWidth: 110,
        gutter:this.gutter,
        column: [
          {
            label: '姓名',
            prop: 'name',
          },
          {
            label: '默认值',
            prop: 'default',
          },
          {
            label: '数字',
            prop: 'number',
            type: 'number',
            rules: [
              {
                required: true,
                message: '请输入数字',
                trigger: 'blur'
              }
            ]
          }
        ]
       }
    }
  },
  methods:{
    handleSubmit(form){
       this.$message.success(JSON.stringify(this.form))
    }
  }
}
</script>
# 栏距离
可以设置每个栏之间的距离,让表单显的紧凑

## 普通用法 
:::demo  设置`gutter`属性调节栏之间的距离
```html
<avue-slider v-model="gutter"></avue-slider>
<avue-form :option="option" v-model="form" @submit="handleSubmit"></avue-form>
<script>
export default {
  data(){
    return {
       gutter:20,
       form:{}
    }
  },
  computed:{
    option(){
      return {
        card: false,
        labelWidth: 110,
        gutter:this.gutter,
        column: [
          {
            label: '姓名',
            prop: 'name',
          },
          {
            label: '默认值',
            prop: 'default',
          },
          {
            label: '数字',
            prop: 'number',
            type: 'number',
            rules: [
              {
                required: true,
                message: '请输入数字',
                trigger: 'blur'
              }
            ]
          }
        ]
       }
    }
  },
  methods:{
    handleSubmit(form){
       this.$message.success(JSON.stringify(this.form))
    }
  }
}
</script>

```
:::

