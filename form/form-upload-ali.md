<script>
export default {
   data(){
     return {
       form:{},
       option: {
          labelWidth: 120,
          column: [
             {
              label: '阿里上传',
              prop: 'imgUrl',
              type: 'upload',
              listType: 'picture-img',
              canvasOption: {
                text: 'avue',
                ratio: 0.1
              },
              oss: 'ali',
              loadText: '附件上传中，请稍等',
              span: 24,
              tip: '只能上传jpg/png文件，且不超过500kb',
            }
          ]
        }
     }
   }
}
</script>

# 阿里云oss上传



```
<!-- 导入需要的包 -->
<script src="https://avuejs.com/cdn/aliyun-oss-sdk.min.js"></script>
<!-- 并且需要入口处全局配置阿里云的参数 -->
Vue.use(AVUE, {
  ali: {
    region: 'oss-cn-beijing',
    endpoint: 'oss-cn-beijing.aliyuncs.com',
    stsToken:'',
    accessKeyId: '',
    accessKeySecret: '',
    bucket: 'avue',
  }
})
```

:::tip
 1.0.6+
::::


:::demo 
```html
<avue-form :option="option" v-model="form"> </avue-form>
<script>
export default {
   data(){
     return {
       form:{},
       option: {
          labelWidth: 120,
          column: [
            {
              label: '阿里上传',
              prop: 'imgUrl',
              type: 'upload',
              listType: 'picture-img',
              canvasOption: {
                text: 'avue',
                ratio: 0.1
              },
              oss: 'ali',
              loadText: '附件上传中，请稍等',
              span: 24,
              tip: '只能上传jpg/png文件，且不超过500kb',
            }
          ]
        }
     }
   }
}
</script>

```
:::

