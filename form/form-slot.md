<script>
export default {
    data() {
      return {
        form:{
          text:''
        },
        option:{
          labelWidth: 120,
          column: [{
              label: '姓名',
              prop: 'text',
              formslot:true,
              labelslot:true,
              errorslot:true,
              rules: [{
                required: true,
                message: "请输入姓名",
                trigger: "blur"
              }]
          }]
        }
      };
    }
}
</script>

# 表单自定义
:::tip
 1.0.0+
::::

## 普通用法

:::demo  
```html
<avue-form v-model="form" :option="option">
  <template slot-scope="scope" slot="text">
    <div>
      <el-tag>{{form.text?form.text:'暂时没有内容'}}</el-tag>
      <el-input v-model="form.text" placeholder="这里是自定的表单"></el-input>
    </div>
  </template>
   <template slot-scope="{}" slot="textLabel">
     <span>姓名&nbsp;&nbsp;</span>
     <el-tooltip class="item" effect="dark" content="文字提示" placement="top-start">
      <i class="el-icon-warning"></i>
    </el-tooltip>
  </template>
   <template slot-scope="{error}" slot="textError">
      <p style="color:green">自定义提示{{error}}</p>
  </template>
</avue-form>

<script>
export default {
    data() {
      return {
        form:{
          text:''
        },
        option:{
          labelWidth: 120,
          column: [{
              label: '姓名',
              prop: 'text',
              formslot:true,
              labelslot:true,
              errorslot:true,
              rules: [{
                required: true,
                message: "请输入姓名",
                trigger: "blur"
              }]
          }]
        }
      };
    }
}
</script>
```
:::