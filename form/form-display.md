<script>
export default {
  data(){
    return {
       form:{
          text1:0,
       },
       option:{
          column: [{
            label: '内容1',
            prop: 'text1',
            type:'radio',
            dicData:[{
              label:'显示',
              value:0
            },{
              label:'隐藏',
              value:1,
            }]
          },{
            label: '内容2',
            prop: 'text2',
            display:true,
            rules: [],
          }]
       }
    }
  },
  watch:{
    'form.text1':{
      handler(val){
        var text2 =this.findObject(this.option.column,'text2')
        if(val===0){
          text2.display=true
          text2.rules=[{
            required: true,
            message: "请输入内容2",
            trigger: "blur"
          }]
        }else{
          text2.display=false
          text2.rules=[]
        }
      },
      immediate: true
    },
  }
}
</script>

# 动态显隐

根据一项的选择决定是否显示其他项

## 普通用法
:::demo  根据`watch`监听对象属性的去做逻辑,调用组件内部方法去`findObject`根据`prop`去寻找配置属性
```html
<avue-form :option="option" ref="form" v-model="form"></avue-form>
<script>
export default {
  data(){
    return {
       form:{
          text1:0,
       },
       option:{
          column: [{
            label: '内容1',
            prop: 'text1',
            type:'radio',
            dicData:[{
              label:'隐藏',
              value:0
            },{
              label:'显示',
              value:1,
            }]
          },{
            label: '内容2',
            prop: 'text2',
            display:true,
            rules: [],
          }]
       }
    }
  },
  watch:{
    'form.text1':{
      handler(val){
        var text2 =this.findObject(this.option.column,'text2')
        if(val===0){
          text2.display=true
          text2.rules=[{
            required: true,
            message: "请输入内容2",
            trigger: "blur"
          }]
        }else{
          text2.display=false
          text2.rules=[]
        }
      },
      immediate: true
    },
  }
}
</script>

```
:::

