# 表单

采用和form组件用法一样，只要切换avue和avue-mobile的包即可，现在手机版只有form组件，后期将会逐步完善

<iframe src="https://sandbox.runjs.cn/show/suarwlhf"  style="margin-top:30px" frameborder="no" border="1" width="376px" height="600px"/>

```
<div id="app">
  <avue-form :option="option" v-model="obj" @submit="submit"></avue-form>
</div>
<script>
let baseUrl = 'https://cli.avuejs.com/api/area'
  new Vue({
    el: '#app',
    data() {
      return {
        obj: {
          name: '11'
        },
        option: {
          column: [
            {
              // prefixIcon: 'contact',
              // suffixIcon: 'question',
              label: '姓名',
              prop: 'name',
              rules: [
                {
                  required: true,
                  message: '请选择姓名',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '密码',
              prop: 'password',
              type: 'password',
              rules: [
                {
                  required: true,
                  message: '请选择密码',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '省份',
              prop: 'province',
              type: 'select',
              props: {
                label: 'name',
                value: 'code'
              },
              cascaderItem: ['city', 'area'],
              cascaderChange: true,
              dicUrl: `${baseUrl}/getProvince`,
              dicData: 'province',
              rules: [
                {
                  required: true,
                  message: '请选择省份',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '城市',
              prop: 'city',
              type: 'select',
              props: {
                label: 'name',
                value: 'code'
              },
              cascaderIndex: 0,
              dicUrl: `${baseUrl}/getCity/{{key}}`,
              rules: [
                {
                  required: true,
                  message: '请选择城市',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '地区',
              prop: 'area',
              type: 'select',
              props: {
                label: 'name',
                value: 'code'
              },
              cascaderIndex: 0,
              dicUrl: `${baseUrl}/getArea/{{key}}`,
              row: true,
              rules: [
                {
                  required: true,
                  message: '请选择地区',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '开关',
              prop: 'switch',
              type: 'switch',
              dicData: [
                {
                  label: '关闭',
                  value: 0
                },
                {
                  label: '启动',
                  value: 1
                }
              ],
              rules: [
                {
                  required: true,
                  message: '请选择开关',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '日期时间',
              prop: 'datetime',
              type: 'datetime',
              format: 'yyyy年MM月dd日 hh时mm分ss秒',
              rules: [
                {
                  required: true,
                  message: '请选择文本',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '日期',
              prop: 'date',
              type: 'date',
              format: 'yyyy年MM月dd日',
              rules: [
                {
                  required: true,
                  message: '请选择日期',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '时间',
              prop: 'time',
              type: 'time',
              row: true,
              format: 'hh小时mm分',
              rules: [
                {
                  required: true,
                  message: '请选择时间',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '单选',
              prop: 'radio',
              type: 'radio',
              tip: '这是一个小提示',
              tags: true,
              dicData: [
                {
                  label: '单程',
                  value: 0
                },
                {
                  label: '往返',
                  value: 1
                }
              ],
              rules: [
                {
                  required: true,
                  message: '请选择单选',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '多选',
              prop: 'checkbox',
              type: 'checkbox',
              tip: '这是一个小提示',
              tags: true,
              row: true,
              dicData: [
                {
                  label: '飞机',
                  value: 1
                },
                {
                  label: '火车',
                  value: 2
                },
                {
                  label: '动车',
                  value: 3
                },
                {
                  label: '其他',
                  value: 4
                }
              ],
              rules: [
                {
                  required: true,
                  message: '请选择多选',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '文本',
              prop: 'textarea',
              type: 'textarea',
              minRows: 3,
              maxRows: 4,
              rules: [
                {
                  required: true,
                  message: '请选择文本',
                  trigger: 'blur'
                }
              ]
            }
          ]
        }
      }
    },
    mounted() {
      setTimeout(() => {
        this.obj = {
          password: '111',
          province: '130000',
          city: '130100',
          area: '130101',
          radio: 0,
          checkbox: [1, 3],
          switch: 1,
          datetime: '2019-01-01 23:22:22',
          date: '2019-01-01',
          time: '03:22'
        }
      }, 0)
    },
    methods: {
      submit() {
        this.$toast(JSON.stringify(this.obj))
      }
    }
  })
</script>

```