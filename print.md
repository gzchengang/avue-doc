<script>
export default {
  methods: {
     handlePrint1(){
      setTimeout(()=>{
        this.$Print({
          id:'test'
        });
      },1000)
    },
    handlePrint() {
      setTimeout(()=>{
        this.$Print({
          html:document.body.innerHTML
        });
      },1000)
    }
  }
}
</script>

# Print打印

:::tip
 2.5.1+
::::

- 进来需要延迟再调用打印

:::demo
```html
<el-button type="primary" @click="handlePrint1" >打印局部</el-button>
<el-button type="primary" @click="handlePrint" >打印全部</el-button>
<div id="test">
  <h2 style="color:red">我是测试字段</h2>
</div>
<script>
export default {
  methods: {
     handlePrint1(){
      setTimeout(()=>{
        this.$Print({
          id:'test'
        });
      },0)
    },
    handlePrint() {
      setTimeout(()=>{
        this.$Print({
          html:document.body.innerHTML
        });
      },0)
    }
  }
}
</script>

```
:::



## Variables

|参数|说明|类型|可选值|默认值|
|-------------|-------------------------------------------------------------|--------|------|------|
|id|dom元素的id|String|-|-|
|html|html代码片段|String|-|-|