<script>
export default {
    data() {
      return {
        reload:false,
        loading:true,
        data: [],
        option:{
         
        },
      };
    },
    mounted(){
      this.$message.success('模拟2s后服务端动态加载');
      setTimeout(()=>{
        this.option={
          border:true,
          page:false,
          align:'center',
          menuAlign:'center',
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        }
        this.data=[
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }
        ]
        this.$nextTick(()=>{
          this.reload=true;
          this.loading=false;
        })
       
      },2000)
    }
}
</script>


# 服务端动态加载配置
:::tip
 2.3.0+
::::




:::demo 如果有服务端字典，需要重新渲染一下v-if组件
```html
<avue-crud :data="data" v-if="reload" :option="option" :table-loading="loading"></avue-crud>

<script>
export default {
    data() {
      return {
        reload:false,
        loading:true,
        data: [],
        option:{
         
        },
      };
    },
    mounted(){
      this.$message.success('模拟2s后服务端动态加载');
      setTimeout(()=>{
        this.option={
          border:true,
          page:false,
          align:'center',
          menuAlign:'center',
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        }
        this.data=[
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }
        ]
        this.$nextTick(()=>{
          this.reload=true;
          this.loading=false;
        })
       
      },2000)
    }
}
</script>
```
:::