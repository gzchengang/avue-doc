<script>
export default {
  data(){
    return {
      row:{},
       data:[{
          id:0,
          name:'张三',
          sex:1,
          $cellEdit:true
       },{
          id:1,
          name:'李四',
          sex:0,
       }],
        option1:{
          addBtn:false,
          editBtn:false,
          addRowBtn:true,
          cellBtn:false,
          cancelBtn:false,
          column: [{
              label:'姓名',
              prop: 'name',
              cell: true,
              rules: [
                {
                  required: true,
                  message: '请输入姓名',
                  trigger: 'blur'
                }
              ]
          },{
              label:'性别',
              prop: 'sex',
              type:'select',
              dicData:[{
                label:'男',
                value:0
              },{
                label:'女',
                value:1
              }],
              cell: true
          },{
              label:'开关',
              prop: 'switch',
              type:'switch',
              cell: true
          }]
        },
       option:{
        addBtn:false,
        editBtn:false,
        addRowBtn:true,
        cellBtn:true,
        menuWidth:250,
        column: [{
            label:'姓名',
            prop: 'name',
            cell: true,
            rules: [
              {
                required: true,
                message: '请输入姓名',
                trigger: 'blur'
              }
            ]
        },{
            label:'性别',
            prop: 'sex',
            type:'select',
            dicData:[{
              label:'男',
              value:0
            },{
              label:'女',
              value:1
            }],
            cell: true
        },{
            label:'开关',
            prop: 'switch',
            type:'switch',
            cell: true
        }]
      }
    }
  },
  methods:{
     rowCell(row, index) {
        this.$refs.crud.rowCell(row, index)
      },
      rowUpdate(form, index, done) {
        this.$message.success(
          '编辑数据' + JSON.stringify(form) + '数据序号' + index
        )
        done()
      },
    addUpdate(form,index,done,loading){
      this.$message.success('模拟网络请求')
      setTimeout(() => {
        this.$message.success('关闭按钮等待')
        loading()
      }, 1000)
      setTimeout(() => {
        this.$message.success(
          '编辑数据' + JSON.stringify(form) + '数据序号' + index
        )
        done()
      }, 2000)
    },
    addRow() {
      this.$message.success('正在添加，请稍后')
      setTimeout(() => {
        for (let i = 0; i < 10; i++) {
          this.$refs.crud.rowCellAdd({
            name: '',
          });
        }
      }, 500)
    },
    addNextRow(index){
      this.data.splice(index+1,0,{
         $cellEdit:true
      })
    },
    addBreakRow(index){
      this.data.splice(index==0?0:(index-1),0,{
         $cellEdit:true
      })
    },
  }
}
</script>

# 表格批量操作

可以批量对表格编辑和新增等操作

## 普通用法
:::demo  配置数据中`$cellEdit`为`true`即可开启首次编辑`addRowBtn`为行新增按钮，`cellBtn`设置为true则开启行编辑按钮，在配置中将编辑的字段设置`cell`为`true`,增删改查方法和`crud`组件使用一致，`rowKey`为主键的key，用于检测是否添加成功或失败。
```html

<avue-crud ref="crud" :option="option" :data="data" @row-update="addUpdate">
  <template slot="menuLeft">
    <el-button @click="addRow" size="small">添加10条</el-button>
  </template>
   <template slot="menu" slot-scope="{row,index,size,type}">
    <el-button @click="addBreakRow(index)" :size="size" :type="type">向上添加</el-button>
    <el-button @click="addNextRow(index)" :size="size" :type="type">向下添加</el-button>
  </template>
</avue-crud>
<script>
export default {
  data(){
    return {
       data:[{
          id:0,
          name:'张三',
          sex:1,
          $cellEdit:true
       },{
          id:1,
          name:'李四',
          sex:0,
       }],
       option:{
        addBtn:false,
        editBtn:false,
        addRowBtn:true,
        cellBtn:true,
        menuWidth:250,
        column: [{
            label:'姓名',
            prop: 'name',
            cell: true,
            rules: [
              {
                required: true,
                message: '请输入姓名',
                trigger: 'blur'
              }
            ]
        },{
            label:'性别',
            prop: 'sex',
            type:'select',
            dicData:[{
              label:'男',
              value:0
            },{
              label:'女',
              value:1
            }],
            cell: true
        },{
            label:'开关',
            prop: 'switch',
            type:'switch',
            cell: true
        }]
      }
    }
  },
  methods:{
    addUpdate(form,index,done,loading){
      this.$message.success('模拟网络请求')
      setTimeout(() => {
        this.$message.success('关闭按钮等待')
        loading()
      }, 1000)
      setTimeout(() => {
        this.$message.success(
          '编辑数据' + JSON.stringify(form) + '数据序号' + index
        )
        done()
      }, 2000)
    },
    addRow() {
      this.$message.success('正在添加，请稍后')
      setTimeout(() => {
        for (let i = 0; i < 10; i++) {
          this.$refs.crud.rowCellAdd({
            name: '',
          });
        }
      }, 500)
    },
    addNextRow(index){
      this.data.splice(index+1,0,{
        $cellEdit:true
      })
    },
    addBreakRow(index){
      this.data.splice(index==0?0:(index-1),0,{
         $cellEdit:true
      })
    },
  }
}
</script>

```
:::


## 自定义用法
:::demo  `cancelBtn`为取消按钮，卡槽中的`row.$cellEdit`来判断是`自定义保存`:`自定义修改`
```html

<avue-crud ref="crud" :option="option1" :data="data" @row-update="addUpdate">
  <template slot="menuLeft">
    <el-button @click="addRow" size="small">添加10条</el-button>
  </template>
  <template slot-scope="{row,index}" slot="menu">
    <el-button
      type="text"
      size="small"
      @click="rowCell(row,index)"
      >自定义</el-button
    >
  </template>
</avue-crud>
<script>
export default {
  data(){
    return {
       data:[{
          id:0,
          name:'张三',
          sex:1,
          $cellEdit:true
       },{
          id:1,
          name:'李四',
          sex:0,
       }],
       option1:{
        addBtn:false,
        editBtn:false,
        addRowBtn:true,
        cellBtn:false,
        cancelBtn:false,
        column: [{
            label:'姓名',
            prop: 'name',
            cell: true,
            rules: [
              {
                required: true,
                message: '请输入姓名',
                trigger: 'blur'
              }
            ]
        },{
            label:'性别',
            prop: 'sex',
            type:'select',
            dicData:[{
              label:'男',
              value:0
            },{
              label:'女',
              value:1
            }],
            cell: true
        },{
            label:'开关',
            prop: 'switch',
            type:'switch',
            cell: true
        }]
      }
    }
  },
  methods:{
     rowCell(row, index) {
        this.$refs.crud.rowCell(row, index)
      },
      rowUpdate(form, index, done) {
        this.$message.success(
          '编辑数据' + JSON.stringify(form) + '数据序号' + index
        )
        done()
      }
  }
}
</script>

```
:::




