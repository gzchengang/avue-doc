<script>
export default {
    data() {
      return {
        data: [
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }
        ],
        option:{
          align:'center',
          menuAlign:'center',
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex',
              order:1
            }
          ]
        },
      };
    },
    created(){
     
    }
}
</script>



# 表单排序
:::tip
 2.6.15+
::::

## 普通用法

:::demo 配置`order`可以改变表单的排序
```html
<avue-crud :data="data" :option="option" ></avue-crud>
<script>
export default {
    data() {
      return {
        data: [
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }
        ],
        option:{
          align:'center',
          menuAlign:'center',
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex',
              order:1
            }
          ]
        },
      };
    },
    created(){
      
    }
}
</script>
```
:::