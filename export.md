<script>
export default {
  data(){
    return {}
  },
  methods: {
     handleExcel() {
      let opt = {
        title: '文档标题',
        column: [{
          label: '标题',
          prop: 'title'
        }],
        data: [{
          title: "测试数据1"
        }, {
          title: "测试数据2"
        }]
      }
      this.$export.excel({
        title: opt.title || new Date().getTime(),
        columns: opt.column,
        data: opt.data
      });
    }
  }
}
</script>

# Export excel导出

:::tip
 2.0.3+
::::

```
<!-- 导入需要的包 （一定要放到head标签里）-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.8/FileSaver.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.14.1/xlsx.full.min.js"></script>
```

:::demo 
```html
<div style="width:400px">
 <el-button type="primary" @click="handleExcel">下载 excel</el-button>
</div>
<script>
export default {
  data(){
    return {}
  },
  methods: {
     handleExcel() {
      let opt = {
        title: '文档标题',
        column: [{
          label: '标题',
          prop: 'title'
        }],
        data: [{
          title: "测试数据1"
        }, {
          title: "测试数据2"
        }]
      }
      this.$export.excel({
        title: opt.title || new Date().getTime(),
        columns: opt.column,
        data: opt.data
      });
    }
  }
}
</script>

```
:::



## Variables

|参数|说明|类型|可选值|默认值|
|-------------|-------------------------------------------------------------|--------|------|------|
|title|水印的文字|String|-|new Date().getTime()|
|column|数据列|Array|-|-|
|data|数据|Array|-|-|