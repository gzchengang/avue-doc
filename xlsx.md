<script>
export default {
  data(){
    return {
      list:[],
      option:{
        column:[{
          label:'id',
          prop:'id'
        },{
          label:'姓名',
          prop:'name'
        },{
          label:'年龄',
          prop:'sex'
        }]
      }
    }
  },
  methods: {
    handleChange(file, fileLis) {
      this.$export.xlsx(file.raw)
        .then(data => {
          this.list=data.results;
        })
    },
    handleGet(){
      window.open('/cdn/demo.xlsx')
    }
  }
}
</script>

# Export xlsx导入

:::tip
 2.1.0+
::::


```
<!-- 导入需要的包 （一定要放到head标签里）-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.8/FileSaver.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.14.1/xlsx.full.min.js"></script>
```

:::demo 
```html
<div style="display:flex;">
 <el-button type="primary" @click="handleGet" >下载模版</el-button>
 <div style="width:20px;"></div>
 <el-upload :show-file-list="false" action="action" :on-change="handleChange">
  <el-button type="primary">导入 excel</el-button>
 </el-upload>
</div>
<br />
<avue-crud :option="option" :data="list"></avue-crud>
<script>
export default {
  data(){
    return {
      list:[],
      option:{
        column:[{
          label:'id',
          prop:'id'
        },{
          label:'姓名',
          prop:'name'
        },{
          label:'年龄',
          prop:'sex'
        }]
      }
    }
    }
  },
  methods: {
    handleGet(){
      window.open('/cdn/demo.xlsx')
    },
    handleChange(file, fileLis) {
      this.$export.xlsx(file.raw)
        .then(data => {
          this.list=data.results;
        })
    }
  }
}
</script>

```
:::


